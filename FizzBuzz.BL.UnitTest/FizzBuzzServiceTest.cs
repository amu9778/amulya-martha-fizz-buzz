﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.BL.Rules;
using FizzBuzz.BL.Interface;
using Moq;

namespace FizzBuzz.BL.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetFizzBuzzRecord_WithInvalidInput_ReturnsEmptyResult()
        {
            //Arrange
            var emptylist = new List<string>();
            var ruleCheck = new Mock<IRuleChecking>();
            var service = new FizzBuzzService(ruleCheck.Object);

            //Act
            var record = service.GetFizzBuzzRecord(0);
            //Assert
            Assert.AreEqual(record.Count, emptylist.Count);
        }
        [TestMethod]
        public void GetFizzBuzzRecord_WithValidInput_ReturnsFizz()
        {
            //Arange
            var number = 3;
            var fizzBuzzRuleMock = CreateRuleMock(3);
            var expectedResult = new List<string> { "1", "2", "Fizz" };
            var ruleCheck = new Mock<IRuleChecking>();
            ruleCheck.Setup(p => p.GetRule(3)).Returns(new FizzRule());
            var service = new FizzBuzzService(ruleCheck.Object);
            //Act

            var records = service.GetFizzBuzzRecord(number);
            //Assert

            Assert.AreEqual(records.Count, number);
            CollectionAssert.AreEqual(records, expectedResult);
        }
        [TestMethod]
        public void GetFizzBuzzRecord_WithValidInput_ReturnsFizzBuzz()
        {
            //Arrange
            var inputNumber = 5;
            var fizzBuzzRuleMock = CreateRuleMock(5);
            var expectedResult = new List<string> { "1", "2", "3", "4", "Buzz" };
            var ruleCheckMock = new Mock<IRuleChecking>();
            ruleCheckMock.Setup(p => p.GetRule(5)).Returns(new BuzzRule());
            var service = new FizzBuzzService(ruleCheckMock.Object);

            //Act
            var records = service.GetFizzBuzzRecord(inputNumber);

            //Assert
            Assert.AreEqual(records.Count, inputNumber);
            CollectionAssert.AreEqual(records.ToList(), expectedResult);


        }

        [TestMethod]
        public void GetFizzBuzzRecord_WithValidNumber_ReturnsFizzBuzz()
        {
            //Arrange
            var number = 5;
            var fizzBuzzRuleMock = CreateRuleMock(5);
            var expactedResult = new List<string> { "1", "2", "Fizz", "4", "Buzz" };
            var ruleCheckMock = new Mock<IRuleChecking>();
            ruleCheckMock.Setup(p => p.GetRule(5)).Returns(new FizzBuzzRule());
            var service = new FizzBuzzService(ruleCheckMock.Object);


            //Act
            var records = service.GetFizzBuzzRecord(number);

            //Assert
            Assert.AreEqual(records.Count, number);
            CollectionAssert.AreEqual(records, expactedResult);

        }
        private IList<IRule> CreateRuleMock(int number)
        {
            var rules = new List<IRule>();


            if (number % 3 == 0)
            {
                var fizzRuleMock = new Mock<IRule>();
                fizzRuleMock.Setup(p => p.IsMatched(number)).Returns(true);
                fizzRuleMock.Setup(p => p.Print()).Returns(DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "Wizz" : "Fizz");
                rules.Add(fizzRuleMock.Object);
            }
            if (number % 5 == 0)
            {
                var buzzRuleMock = new Mock<IRule>();
                buzzRuleMock.Setup(p => p.IsMatched(number)).Returns(true);
                buzzRuleMock.Setup(p => p.Print()).Returns(DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "Wuzz" : "Buzz");
                rules.Add(buzzRuleMock.Object);
            }
            if (number % 3 == 0 && number % 5 == 0)
            {
                var fizzbuzzRuleMock = new Mock<IRule>();
                fizzbuzzRuleMock.Setup(p => p.IsMatched(number)).Returns(true);
                fizzbuzzRuleMock.Setup(p => p.Print()).Returns(DateTime.Now.DayOfWeek == DayOfWeek.Wednesday ? "WizzWuzz" : "FizzBuzz");
                rules.Add(fizzbuzzRuleMock.Object);
            }
            return rules;
        }

        }
    }
