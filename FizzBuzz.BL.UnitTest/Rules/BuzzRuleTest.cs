﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.BL.Rules;

namespace FizzBuzz.BL.UnitTest.Rules
{
    [TestClass]
    public class BuzzRuleTest
    {
        [TestMethod]

        public void IsMatched_WhenRuleMatched_RetrunsTrue()
        {
            //Arrange
            var buzzrule = new BuzzRule();
            //Act
            var result = buzzrule.IsMatched(5);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotMatched_WhenRuleNotMatchd_ReturnsFalse()
        {
            //Arrange
            var buzzRule = new BuzzRule();
            //Act
            var result = buzzRule.IsMatched(11);
            //Assert
            Assert.IsFalse(result);
        }

    }
}
