﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.BL.Rules;
namespace FizzBuzz.BL.UnitTest.Rules
{
    [TestClass]
    public class FizzBuzzRuleTest
    {
        [TestMethod]

        public void IsMatched_WhenRuleMatched_RetrunsTrue()
        {
            //Arrange
            var fizzbuzzrule = new FizzBuzzRule();
            //Act
            var result = fizzbuzzrule.IsMatched(15);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotMatched_WhenRuleNotMatchd_ReturnsFalse()
        {
            //Arrange
            var fizzbuzzrule = new FizzBuzzRule();
            //Act
            var result = fizzbuzzrule.IsMatched(11);
            //Assert
            Assert.IsFalse(result);
        }

    }
}
