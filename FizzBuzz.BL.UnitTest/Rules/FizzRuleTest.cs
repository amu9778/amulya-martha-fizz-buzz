﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.BL.Rules;

namespace FizzBuzz.BL.UnitTest.Rules
{
    [TestClass]
    public class FizzRuleTest
    {
        [TestMethod]
        
       public void IsMatched_WhenRuleMatched_RetrunsTrue()
        {
            //Arrange
            var fizzrule = new FizzRule();
            //Act
            var result = fizzrule.IsMatched(3);
            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void IsNotMatched_WhenRuleNotMatchd_ReturnsFalse()
        {
            //Arrange
            var fizzrule = new FizzRule();
            //Act
            var result = fizzrule.IsMatched(11);
            //Assert
            Assert.IsFalse(result);
        }

        
    }
}
