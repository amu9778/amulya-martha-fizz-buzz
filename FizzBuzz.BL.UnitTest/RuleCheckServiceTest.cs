﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.BL.Rules; 

namespace FizzBuzz.BL.UnitTest
{
    [TestClass]
    public class RuleCheckServiceTest
    {
        [TestMethod]
        public void GetRule_WhenNotMatched_ReturnsNoRule()
        {
            //Arrange
            var ruleChecker = new RuleCheckService();

            //Act
            var matchedRules = ruleChecker.GetRule(2);

            //Assert
            Assert.IsNull(matchedRules);
        }

        [TestMethod]
        public void GetRule_WhenMatched_ReturnsFizzBuzzRule()
        {
            //Arrange
            var ruleChecker = new RuleCheckService();

            //Act
            var matchedRules = ruleChecker.GetRule(15);

            //Assert
            Assert.AreEqual(matchedRules.ToString(), new FizzBuzzRule().ToString());
        }
        [TestMethod]
        public void GetRule_WhenMatched_ReturnsBuzzRule()
        {
            //Arrange
            var ruleChecker = new RuleCheckService();

            //Act
            var matchedRule = ruleChecker.GetRule(5);

            //Assert
            Assert.AreEqual(matchedRule.ToString(), new BuzzRule().ToString());
        }
        [TestMethod]
        public void GetRule_WhenMatched_ReturnsFizzRule()
        {
            //Arrange
            var ruleChecker = new RuleCheckService();

            //Act
            var matchedRule = ruleChecker.GetRule(3);

            //Assert
            Assert.AreEqual(matchedRule.ToString(), new FizzRule().ToString());
        }
    }
}
