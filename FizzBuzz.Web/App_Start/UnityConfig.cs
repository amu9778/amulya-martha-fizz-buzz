using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using FizzBuzz.BL.Interface;
using FizzBuzz.BL;
namespace FizzBuzz.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<IFizzBuzzService, FizzBuzzService>();
            container.RegisterType<IRuleChecking, RuleCheckService>();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}