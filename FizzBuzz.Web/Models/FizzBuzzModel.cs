﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzModel
    {
        [Required]
        [RegularExpression("([0-9]+)",ErrorMessage ="Please Enter a Valid Number")]
        [Range(1,1000,ErrorMessage ="Value should in between {1} and {2}")]
        public int Number { get; set; }
        public List<string> Records { get; set; }
    }
}