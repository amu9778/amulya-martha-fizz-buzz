﻿using FizzBuzz.BL.Interface;
using FizzBuzz.Web.Models;
using System.Web.Mvc;
using System.Collections.Generic;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFizzBuzzService _fizzBuzzService;
        
        public HomeController(IFizzBuzzService fizzBuzzService)
        {
            _fizzBuzzService = fizzBuzzService;
             
        }
        public ActionResult Index(FizzBuzzModel model)
        {
            if (ModelState.IsValid)
            {
                model.Records = _fizzBuzzService.GetFizzBuzzRecord(model.Number).ToList();
            }
            return View(model: model);
        }

 
    }
}