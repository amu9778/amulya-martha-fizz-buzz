﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unity;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using FizzBuzz.BL.Interface;
using System.Web.Mvc;
using System.Collections.Generic;

namespace FizzBuzz.Web.UnitTest
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index_WhenInvalidNumber_ReturnsEmpty()
        {
            //Arrange
            var container = new UnityContainer();
            var fizzBuzzService = container.Resolve<IFizzBuzzService>();
            var ruleCheckService = container.Resolve<IRuleChecking>();
            HomeController controller = new HomeController(fizzBuzzService);
            var fizzBuzzViewModel = new FizzBuzzModel { Number = -6 };

            //Act
            ViewResult result = controller.Index(fizzBuzzViewModel) as ViewResult;
            var model = result.Model as FizzBuzzModel;

            //Assert
            Assert.AreEqual(model.Records.Count, 0);

        }
        [TestMethod]
        public void Index_WhenValidNumber_ReturnsRecord()
        {
            //Arrange
            var container = new UnityContainer();
            var fizzBuzzService = container.Resolve<IFizzBuzzService>();
            var ruleCheckService = container.Resolve<IRuleChecking>();
            HomeController controller = new HomeController(fizzBuzzService);
            var fizzBuzzViewModel = new FizzBuzzModel { Number = 4 };
            var expected = new List<string> { "1", "2", "Fizz", "4" };

            //Act
            ViewResult result = controller.Index(fizzBuzzViewModel) as ViewResult;
            var model = result.Model as FizzBuzzModel;

            //Assert
            Assert.AreEqual(model.Records.Count, fizzBuzzViewModel.Number);
            Assert.AreEqual(model.Records.Count, 4);
            CollectionAssert.AreEqual(model.Records.ToList<string>(), expected.ToList<string>());
        }

    }
}
