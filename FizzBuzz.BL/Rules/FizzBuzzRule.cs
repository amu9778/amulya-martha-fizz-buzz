﻿using System;
using FizzBuzz.BL.Interface;

namespace FizzBuzz.BL.Rules
{
    public class FizzBuzzRule:IRule
    {
        public bool IsMatched(int Number)
        {
            return  Number %3 ==0 && Number % 5 == 0;
        }

        public string Print()
        {
            if (System.DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                return "WizzWuzz";
            }
            else
            {
                return "FizzBuzz";
            }
        }
    }
}
