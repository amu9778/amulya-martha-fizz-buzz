﻿using System;
using FizzBuzz.BL.Interface;
namespace FizzBuzz.BL.Rules
{
    public class BuzzRule : IRule
    {
        public bool IsMatched(int Number)
        {
            return Number % 5 == 0;
        }

        public string Print()
        {
            if (System.DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                return "Wuzz";
            }
            else
            {
                return "Buzz";
            }
        }
    }
}
