﻿using System;
using FizzBuzz.BL.Interface;

namespace FizzBuzz.BL.Rules
{
    public class FizzRule : IRule
    {
        public bool IsMatched(int Number)
        {
            return Number % 3 == 0;
        }

        public string Print()
        {
           if(System.DateTime.Today.DayOfWeek==DayOfWeek.Wednesday)
            {
                return "Wizz";
            }
           else
            {
                return "Fizz";
            }
        }
    }
}
