﻿using System.Collections.Generic;
using FizzBuzz.BL.Rules;
using FizzBuzz.BL.Interface;

namespace FizzBuzz.BL
{
    public class RuleCheckService:IRuleChecking 
    {
        public IRule GetRule(int number)
        {
            var rules = new List<IRule> { new FizzBuzzRule(), new FizzRule(), new BuzzRule() };
            foreach (var rule in rules)
            {
                if (rule.IsMatched(number))
                {
                    return rule;
                }
            }
            return null;
        }
    }  

}
