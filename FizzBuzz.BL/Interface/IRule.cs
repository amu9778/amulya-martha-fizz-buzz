﻿namespace FizzBuzz.BL.Interface
{
    public interface IRule
    {
        string Print();
        bool IsMatched(int Number);
    }
}
