﻿
namespace FizzBuzz.BL.Interface
{
    public interface IRuleChecking
    {
        IRule GetRule(int Number);
    }
}
