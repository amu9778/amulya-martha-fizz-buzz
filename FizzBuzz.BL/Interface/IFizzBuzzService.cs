﻿using System.Collections.Generic;


namespace FizzBuzz.BL.Interface
{
   public  interface IFizzBuzzService
    {
        List<string> GetFizzBuzzRecord(int Number);

    }
}
