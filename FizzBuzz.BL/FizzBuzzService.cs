﻿using System.Collections.Generic;
using FizzBuzz.BL.Interface;

namespace FizzBuzz.BL
{
    public class FizzBuzzService:IFizzBuzzService
    {
        private readonly IRuleChecking _ruleCheck;
        public FizzBuzzService(IRuleChecking ruleCheck)
        {
            _ruleCheck = ruleCheck;
        }
        public List<string> GetFizzBuzzRecord(int Number)
        {
            var records = new List<string>();

            for (int i = 1; i <= Number; i++)
            {
                var rules = _ruleCheck.GetRule(i);
                if (rules != null)
                {
                    records.Add(rules.Print());
                }
                else
                {
                    records.Add(i.ToString());
                }

            }
            
            return records;
        }
    }
}
